The <code>stream-sandbox</code> Module
======================================

This project is an open attempt for collectin partially working code and
will be provided in nightly builds without tests being run.

The idea of the *stream-sandbox* is to provide an environment for developing
new processors, learners, data-stream implementations and so on. As soon as
some of the code/classes have become stable and unit-tests and documentation
is provided, the will be moved to the *stream-core* module if they are useful
for general purposes.

Another direction is the development of machine learning algorithms within the
*stream-sandbox* project. Stable learner implementations are thought to be moved
to the *stream-mining* project.
