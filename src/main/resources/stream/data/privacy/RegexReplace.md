RegexReplace
============

This obfuscating processor will replace all occurrences that match a
given regular expression in all attributes by some obfuscated random
string.

If the `keys` parameter is set to a list of keys, then only these
keys will be changed all other keys/attributes remain untouched.
