Obfuscator
==========

This general obfuscating processor inspects data items and will randomly
obfuscate each value of a given set of keys. The the values are also replaced
in all other keys found in the item.

For example, if the obfuscators `keys` parameter is set to `REMOTE_ADDR,USER`,
then the values for `REMOTE_ADDR` and `USER` will be obfuscated and any
occurrences of the original values in any other attribute of the item will
also be replace with the obfuscated value.
