Package stream.data.privacy
===========================

This package provides processors for pseudonymization and
obfuscation of data items.
