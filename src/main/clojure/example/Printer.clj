(ns example.Printer
  (:gen-class
   :state state
   :init init
   :implements [stream.Processor]))

(defn -init []
  (let [out (println "Initializing first clojure-processor...")]
  [[] (ref { :count 0 })]))

(defn update-model [this data]
   (let [cur (.state this)
         ch (dosync (ref-set cur (assoc @cur :count (inc (:count @cur)))))]
     data))

(defn -process [this data]
  (let [o2 (println (str "my state: " @(.state this)))]
    (update-model this data)))

