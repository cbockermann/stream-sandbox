package stream.data;

import java.util.List;

import stream.AbstractProcessor;
import stream.Data;
import stream.ProcessContext;
import stream.service.BufferService;
import stream.service.SimpleBufferService;

public class BufferProcessor extends AbstractProcessor implements BufferService {

	private BufferService bufferService;

	@Override
	public void init(ProcessContext ctx) throws Exception {
		super.init(ctx);
		this.bufferService = new SimpleBufferService();
	}

	@Override
	public Data process(Data input) {
		return input;
	}

	@Override
	public void reset() throws Exception {
		bufferService.reset();

	}

	@Override
	public List<Data> read(String key) {
		return bufferService.read(key);
	}

	@Override
	public void write(String key, Data data) {
		bufferService.write(key, data);
	}

	@Override
	public void remove(String key) {
		bufferService.remove(key);
	}

	@Override
	public void increaseReads(String key, int number) {
		bufferService.increaseReads(key, number);
	}

}
