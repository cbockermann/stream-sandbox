package stream.data;

import stream.AbstractProcessor;
import stream.Data;
import stream.service.BufferService;

public class IncreaseReads extends AbstractProcessor {

	private BufferService bufferService;
	private String key;
	private Integer number;

	public IncreaseReads() {
		this.number = 1;
	}

	public Integer getNumber() {
		return number;
	}

	public void setNumber(Integer number) {
		this.number = number;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public BufferService getBufferService() {
		return bufferService;
	}

	public void setBufferService(BufferService bufferService) {
		this.bufferService = bufferService;
	}

	@Override
	public Data process(Data input) {
		bufferService.increaseReads(key, number);
		return input;
	}

}
