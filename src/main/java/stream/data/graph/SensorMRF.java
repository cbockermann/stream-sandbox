/*
 *  stream.ai
 *
 *  Copyright (C) 2011-2012 by Christian Bockermann, Hendrik Blom
 * 
 *  stream.ai is a library, API and runtime environment for processing high
 *  volume data streams. It is composed of three submodules "stream-api",
 *  "stream-core" and "stream-runtime".
 *
 *  The stream.ai library (and its submodules) is free software: you can 
 *  redistribute it and/or modify it under the terms of the 
 *  GNU Affero General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any 
 *  later version.
 *
 *  The stream.ai library (and its submodules) is distributed in the hope
 *  that it will be useful, but WITHOUT ANY WARRANTY; without even the implied 
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package stream.data.graph;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import stream.AbstractProcessor;
import stream.Data;
import stream.ProcessContext;
import stream.data.DataFactory;
import stream.data.distribution.NominalDistribution;
import stream.data.graph.factor.DiscreteDomain;
import stream.data.graph.factor.DiscreteVariable;
import stream.data.graph.factor.TableFactor;
import stream.io.QueueService;

/**
 * @author nico
 * 
 */
public class SensorMRF extends AbstractProcessor {

	static Logger log = LoggerFactory.getLogger(SensorMRF.class);
	GraphService graphService;

	Map<Integer, Integer> neighbor_map = new HashMap<Integer, Integer>();

	String dataKey = "VALUE";
	String TypeKey = "TYPE";
	String node;
	NominalDistribution<String> emp = new NominalDistribution<String>();

	Integer T;
	Integer d;
	Integer id;
	Integer dom;

	TableFactor[] theta_v;
	TableFactor[][] theta_vu;

	TableFactor[] theta_v_old;
	TableFactor[][] theta_vu_old;

	TableFactor[] b_v;
	TableFactor[][] b_vu;

	TableFactor[][] i_messages;
	TableFactor[][] o_messages;

	QueueService[] channel;

	double bound = 1E-4;
	double damping = 0.0;

	int NODE = 2;
	int EDGE = 2;

	public void init(ProcessContext ctx) throws Exception {
		super.init(ctx);

		d = graphService.getNeighborCount(id);

		theta_v = new TableFactor[this.T];
		theta_v_old = new TableFactor[this.T];
		b_v = new TableFactor[this.T];

		for (int t = 0; t < this.T; ++t) {
			theta_v[t] = new TableFactor(NODE, new DiscreteDomain(NODE,
					new DiscreteVariable(id, dom)));
			theta_v[t].uniform(0.0);
			theta_v_old[t] = new TableFactor(NODE, theta_v[t]);
		}

		theta_vu = new TableFactor[this.T][d];
		theta_vu_old = new TableFactor[this.T][d];
		b_vu = new TableFactor[this.T][d];
		i_messages = new TableFactor[this.T][d];
		o_messages = new TableFactor[this.T][d];

		for (int t = 0; t < this.T; ++t) {
			int i = 0;
			for (Integer ii : graphService.getNeighbors(id)) {
				theta_vu[t][i] = new TableFactor(EDGE, new DiscreteDomain(EDGE,
						new DiscreteVariable(id, dom), new DiscreteVariable(ii,
								dom)));
				theta_vu[t][i].uniform(0.0);
				theta_vu_old[t][i] = new TableFactor(EDGE, theta_vu[t][i]);
				i_messages[t][i] = new TableFactor(EDGE, new DiscreteDomain(
						EDGE, new DiscreteVariable(id, dom)));
				i_messages[t][i].uniform(0.0);
				o_messages[t][i] = new TableFactor(EDGE, new DiscreteDomain(
						EDGE, new DiscreteVariable(id, dom)));
				o_messages[t][i].uniform(0.0);
				++i;
			}
		}

		channel = new QueueService[d];
		int i = 0;
		for (Integer ii : graphService.getNeighbors(id)) {

			neighbor_map.put(ii, i);

			try {
				channel[i] = context.lookup("queue-" + ii.toString(),
						QueueService.class);

				Data msg = DataFactory.create();
				msg.put("FROM", id.toString());
				msg.put(getTypeKey(), "HELLO");

				channel[i].enqueue(msg);

			} catch (Exception e) {
				log.info("INIT: Error while connecting to queue-{}.",
						ii.toString());
			}
			++i;
		}
		log.info("INIT SENSOR v = {}, |X| = {}", graphService.getName(id),
				dom.toString());
	}

	/**
	 * @see stream.Processor#process(stream.Data)
	 */
	@Override
	public Data process(Data input) {

		Serializable type = input.get(getTypeKey());

		if (type == null)
			emp.update(input.get(dataKey).toString());

		else if (type.equals("HELLO")) {
			log.info("NODE {} FOUND BY NODE {}", id.toString(),
					input.get("FROM"));
		}

		else if (type.equals("BP")) {
			Integer t = new Integer(input.get("t").toString());
			Integer ii = new Integer(input.get("FROM").toString());

			TableFactor msg = (TableFactor) input.get("MSG");
			log.info("MSG {} FROM NEIGHBOR {}", t, neighbor_map.get(ii));
			// i_messages[t][neighbor_map.get(ii)] = msg;
		}

		compute_est();

		return input;
	}

	/*
	 * log.info("Hist: {}", emp.getHistogram()); for( String val :
	 * emp.getHistogram().keySet() ){ log.debug( "  prob({}) = {}", val,
	 * emp.prob(val)); }
	 */

	public void update_theta() {
		for (int t = 0; t < T; ++t) {

		}
	}

	public void compute_est() {
		for (int t = 0; t < T; ++t) {

			b_v[t] = new TableFactor(NODE, theta_v[t]);
			for (int i = 0; i < graphService.getNeighborCount(id); ++i) {
				b_v[t].mult(i_messages[t][i]);
			}

			TableFactor cavity, tmp_msg;

			int i = 0;
			for (Integer ii : graphService.getNeighbors(id)) {

				cavity = new TableFactor(NODE, b_v[t]);
				cavity.div(i_messages[t][i]);

				tmp_msg = new TableFactor(NODE, new DiscreteDomain(NODE,
						new DiscreteVariable(ii, dom)));
				tmp_msg.convolve(theta_vu[t][i], cavity);
				tmp_msg.normalize();

				double residual = tmp_msg.l1_diff(o_messages[t][i]);
				tmp_msg.damp(o_messages[t][i], damping);

				if (residual > bound) {
					o_messages[t][i] = tmp_msg;

					Data msg = DataFactory.create();
					msg.put("t", new Integer(t).toString());
					msg.put("FROM", id.toString());
					msg.put(getTypeKey(), "BP");
					msg.put("MSG", tmp_msg);

					try {
						channel[i].enqueue(msg);

					} catch (Exception e) {
						log.info("BP: Error while connecting to queue-{}.",
								ii.toString());
					}
				}
				++i;
			}
		}
	}

	/**
	 * @return the graphService
	 */
	public GraphService getGraph() {
		return graphService;
	}

	/**
	 * @param graphService
	 *            the graphService to set
	 */
	public void setGraph(GraphService graphService) {
		this.graphService = graphService;
	}

	/**
	 * @return the node
	 */
	public String getNode() {
		return node;
	}

	/**
	 * @param node
	 *            the node to set
	 */
	public void setNode(String node) {
		this.node = node;
		this.id = new Integer(node);
	}

	public void setDataKey(String key) {
		this.dataKey = key;
	}

	public String getDataKey() {
		return dataKey;
	}

	public void setTypeKey(String key) {
		this.TypeKey = key;
	}

	public String getTypeKey() {
		return TypeKey;
	}

	public void setT(String T) {
		this.T = new Integer(T);
	}

	public Integer getT() {
		return T;
	}

	public void setDomainSize(String dom) {
		this.dom = new Integer(dom);
	}

	public Integer getDomainSize() {
		return dom;
	}
}
