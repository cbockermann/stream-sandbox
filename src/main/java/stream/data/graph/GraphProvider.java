/*
 *  stream.ai
 *
 *  Copyright (C) 2011-2012 by Christian Bockermann, Hendrik Blom
 * 
 *  stream.ai is a library, API and runtime environment for processing high
 *  volume data streams. It is composed of three submodules "stream-api",
 *  "stream-core" and "stream-runtime".
 *
 *  The stream.ai library (and its submodules) is free software: you can 
 *  redistribute it and/or modify it under the terms of the 
 *  GNU Affero General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any 
 *  later version.
 *
 *  The stream.ai library (and its submodules) is distributed in the hope
 *  that it will be useful, but WITHOUT ANY WARRANTY; without even the implied 
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package stream.data.graph;

import java.io.File;
import java.io.FileInputStream;
import java.io.Serializable;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import stream.Data;
import stream.io.CsvStream;

/**
 * @author chris
 * 
 */
public class GraphProvider implements GraphService {

	static Logger log = LoggerFactory.getLogger(GraphProvider.class);

	Map<Integer, Set<Integer>> neighbors = new HashMap<Integer, Set<Integer>>();
	Map<Integer, String> names = new HashMap<Integer, String>();
	Map<String, Integer> ids = new HashMap<String, Integer>();
	File file;

	Integer nodes = 0;
	Integer edges = 0;

	/**
	 * @return the file
	 */
	public File getFile() {
		return file;
	}

	/**
	 * @param file
	 *            the file to set
	 */
	public void setFile(File file) {
		this.file = file;
	}

	/**
	 * @param id
	 */
	@Override
	public String getName(int id) {
		return names.get(id);
	}

	/**
	 * @param id
	 */
	@Override
	public int getNeighborCount(int id) {
		return neighbors.get(id).size();
	}

	/**
	 * @param id
	 */
	public String getId(String name) throws Exception {
		log.debug("getId({})", name);
		return ids.get(name).toString();
	}

	/**
	 * @see stream.service.Service#reset()
	 */
	@Override
	public void reset() throws Exception {
		log.info("Reading graph from {}", file);
		log.debug("NAME <-> ID");

		CsvStream stream = new CsvStream(new FileInputStream(file));
		Data item = stream.readNext();
		while (!item.get("a").equals("EDGES")) {

			Serializable id = item.get("a");
			Serializable name = item.get("b");

			if (id != null && name != null) {
				addNode(new Integer(id.toString()), name.toString());
			} else {
				throw new Exception(
						"Failed to extract start/end from data item: " + item
								+ "!");
			}
			item = stream.readNext();
		}

		nodes = names.size();
		log.info("|V| = {}", nodes.toString());

		Integer count = 0;
		item = stream.readNext();
		while (item != null) {
			Serializable start = item.get("a");
			Serializable end = item.get("b");

			if (start != null && end != null) {
				addEdge(new Integer(start.toString()),
						new Integer(end.toString()));
			} else {
				throw new Exception(
						"Failed to extract start/end from data item: " + item
								+ "!");
			}
			count++;
			item = stream.readNext();
		}
		edges = count;
		log.info("|E| = {}", edges.toString());

		stream.close();
	}

	public void addNode(int id, String name) {
		names.put(id - 1, name);
		ids.put(name, id - 1);
		log.debug("{} <-> {}", name, id - 1);
	}

	public void addEdge(Integer start, Integer end) {
		start--;
		end--;

		Set<Integer> neighs = neighbors.get(start);
		if (neighs == null) {
			neighs = new LinkedHashSet<Integer>();
			neighbors.put(start, neighs);
		}
		neighs.add(end);
		log.debug("({}, {})", start, end);
	}

	/**
	 * @see stream.data.graph.GraphService#getNodes()
	 */
	@Override
	public Set<Integer> getNodes() {
		return neighbors.keySet();
	}

	/**
	 * @see stream.data.graph.GraphService#getNeighbors(java.lang.String)
	 */
	@Override
	public Set<Integer> getNeighbors(int node) {
		if (neighbors.containsKey(node))
			return neighbors.get(node);
		else
			return new HashSet<Integer>();
	}

}
