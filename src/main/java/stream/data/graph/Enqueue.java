/*
 *  stream.ai
 *
 *  Copyright (C) 2011-2012 by Christian Bockermann, Hendrik Blom
 * 
 *  stream.ai is a library, API and runtime environment for processing high
 *  volume data streams. It is composed of three submodules "stream-api",
 *  "stream-core" and "stream-runtime".
 *
 *  The stream.ai library (and its submodules) is free software: you can 
 *  redistribute it and/or modify it under the terms of the 
 *  GNU Affero General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any 
 *  later version.
 *
 *  The stream.ai library (and its submodules) is distributed in the hope
 *  that it will be useful, but WITHOUT ANY WARRANTY; without even the implied 
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package stream.data.graph;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import stream.AbstractProcessor;
import stream.Data;
import stream.io.QueueService;

/**
 * @author chris
 * 
 */
public class Enqueue extends AbstractProcessor {

	static Logger log = LoggerFactory.getLogger(SensorMRF.class);
	protected GraphService graphService;
	protected QueueService queue;
	protected String node;
	protected String key = "NAME";

	/**
	 * @return the graphService
	 */
	public GraphService getGraph() {
		return graphService;
	}

	/**
	 * @param graphService
	 *            the graphService to set
	 */
	public void setGraph(GraphService graphService) {
		this.graphService = graphService;
	}

	/**
	 * @return the node
	 */
	public String getNode() {
		return node;
	}

	/**
	 * @param node
	 *            the node to set
	 */
	public void setNode(String node) {
		this.node = node;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getKey() {
		return key;
	}

	/**
	 * @see stream.Processor#process(stream.Data)
	 */
	@Override
	public Data process(Data input) {
		try {
			String name = graphService.getId(input.get(key).toString());
			QueueService queue = context.lookup("queue-" + name,
					QueueService.class);
			if (queue != null)
				queue.enqueue(input);

		} catch (Exception e) {
			// log.debug( "Failed to enqueue item: {}", e.getMessage());
		}
		return input;
	}
}
