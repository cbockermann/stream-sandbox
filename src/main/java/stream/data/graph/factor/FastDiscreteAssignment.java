package stream.data.graph.factor;

public class FastDiscreteAssignment {

	private int MAX_DIM;

	private int _num_vars;
	public int _index;
	private DiscreteVariable[] _vars; // actual ordering of the assignments
	private int[] _increment_step; // increment ordering according to _vars
	private int[] _asgs; // assignments with respect to _vars
	protected boolean transposed;

	public FastDiscreteAssignment(int MAX_DIM, DiscreteDomain args) {
		this.MAX_DIM = MAX_DIM;
		_vars = new DiscreteVariable[MAX_DIM];
		_increment_step = new int[MAX_DIM];
		_asgs = new int[MAX_DIM];

		transposed = false;
		_num_vars = args.getNumVars();
		_index = 0;
		for (int i = 0; i < _num_vars; ++i) {
			_vars[i] = args.var(i);
			_asgs[i] = 0;
		}
		int multiple = 1;
		for (int i = 0; i < _num_vars; ++i) {
			_increment_step[i] = multiple;
			multiple *= _vars[i].size();
		}
	}

	// ! Construct a FastDiscreteAssignment from a DiscreteAssignment
	public FastDiscreteAssignment(int MAX_DIM, DiscreteAssignment asg) {
		this.MAX_DIM = MAX_DIM;
		_vars = new DiscreteVariable[MAX_DIM];
		_increment_step = new int[MAX_DIM];
		_asgs = new int[MAX_DIM];

		_index = asg.getLinearIndex();
		transposed = false;
		_num_vars = asg.getArgs().getNumVars();
		for (int i = 0; i < _num_vars; ++i) {
			_vars[i] = asg.getArgs().var(i);
			_asgs[i] = asg.getAsgAt(i);
		}

		int multiple = 1;
		for (int i = 0; i < _num_vars; ++i) {
			_increment_step[i] = multiple;
			multiple *= _vars[i].size();
		}
	}

	// ! get the number of variables
	public int getNumVars() {
		return _num_vars;
	}

	// ! get the number of variables
	public DiscreteVariable var(int i) {
		return _vars[i];
	}

	public int getLinearIndex() {
		return _index;
	}

	// ! Get the next FastDiscreteAssignment
	public FastDiscreteAssignment increment() {
		// Update the DiscreteAssignments
		for (int i = 0; i < getNumVars(); ++i) {
			if (_asgs[i] < (_vars[i].size() - 1)) {
				_asgs[i] = (_asgs[i] + 1);
				_index += _increment_step[i];
				return this;
			} else {
				_index -= _asgs[i] * _increment_step[i];
				_asgs[i] = 0;
			}
		}
		// Reached end
		make_end();
		return this;
	}

	public void set_index(int index) {
		_index = index;
		recomputeAsgs();
	}

	public int asg(int var_id) {
		int idx = varLocation(var_id);
		// assert(idx < _getNumVars());
		return _asgs[idx];
	}

	public void set_asg(int var_id, int value) {
		int idx = varLocation(var_id);
		// assert(idx < getNumVars());
		// assert(value < var(idx).size());
		_asgs[idx] = value;
		recomputeLinearIndex();
	}

	// ! Tests whether two FastDiscreteAssignments are equal
	public boolean eq(FastDiscreteAssignment other) {
		return _index == other._index;
	}

	// ! Tests whether two FastDiscreteAssignments are not equal
	public boolean neq(FastDiscreteAssignment other) {
		return _index != other._index;
	}

	// ! Make this an ending FastDiscreteAssignment
	public void make_end() {
		_index = -1;
	}

	/**
	 * Makes the sub_domain the first set of variables to be incremented over
	 * Can only be called once
	 */
	public void transposeToStart(DiscreteDomain sub_domain) {
		// assert_FALSE(transposed);
		transposed = true;

		int[] reorder_map = new int[MAX_DIM];
		int cursubdomain_idx = 0;
		int remainder_idx = sub_domain.getNumVars();
		for (int i = 0; i < getNumVars(); ++i) {
			if (cursubdomain_idx < sub_domain.getNumVars()
					&& _vars[i].id() == sub_domain.var(cursubdomain_idx).id()) {
				reorder_map[cursubdomain_idx] = i;
				++cursubdomain_idx;
			} else {
				reorder_map[remainder_idx] = i;
				++remainder_idx;
			}
		}
		// move the asg around

		int[] newasgs = new int[MAX_DIM];
		int[] newincrement_step = new int[MAX_DIM];
		DiscreteVariable[] newvars = new DiscreteVariable[MAX_DIM];
		for (int i = 0; i < getNumVars(); ++i) {
			newincrement_step[i] = _increment_step[reorder_map[i]];
			newasgs[i] = _asgs[reorder_map[i]];
			newvars[i] = _vars[reorder_map[i]];
		}
		// copyback
		for (int i = 0; i < getNumVars(); ++i) {
			_asgs[i] = newasgs[i];
			_vars[i] = newvars[i];
			_increment_step[i] = newincrement_step[i];
		}
	} // end of restrict

	// ! Recompute the index from the DiscreteAssignment
	private void recomputeLinearIndex() {
		int multiple = 1;
		// Clear the index
		_index = 0;
		for (int i = 0; i < getNumVars(); ++i) {
			_index += multiple * _asgs[i];
			// //assert(_args.var(i).nasgs > 0);
			multiple *= _vars[i].size();
		}
	}

	// ! Recompute the DiscreteAssignments from the index
	private void recomputeAsgs() {
		int quotient = _index;
		for (int i = 0; i < getNumVars(); ++i) {
			_asgs[i] = quotient % _vars[i].size();
			quotient /= _vars[i].size();
			// //assert(_asgs[i] < _args.var(i).size());
		}
	}

	/**
	 * get the index of the variable or returns number of variables if the index
	 * is not found
	 */
	private int varLocation(int var_id) {
		int location = _num_vars;
		for (int i = 0; i < _num_vars && !(location < _num_vars); ++i) {
			if (_vars[i].id() == var_id)
				location = i;
		}
		return location;
	}

	public String toString() {
		String out = new String("{");
		for (int i = 0; i < _num_vars; ++i) {
			out += "v_" + _vars[i].id();
			if (i < getNumVars() - 1)
				out += ", ";
		}
		out += "}=" + getLinearIndex();
		return out;
	}
}
