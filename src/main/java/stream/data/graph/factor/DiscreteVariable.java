package stream.data.graph.factor;

import java.io.Serializable;

public class DiscreteVariable implements Comparable<DiscreteVariable>,
		Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2268728497981588910L;

	/**
	 * construct a discrte variable with a given id and number of assignments
	 */
	public DiscreteVariable() {
		id_ = 0;
		nasgs_ = 0;
	}

	/**
	 * construct a discrte variable with a given id and number of assignments
	 */
	public DiscreteVariable(int id, int nasgs) {
		id_ = id;
		nasgs_ = nasgs;
	}

	public int compareTo(DiscreteVariable other) {
		if (id_ < other.id())
			return -1;
		if (id_ > other.id())
			return 1;
		return 0;
	}

	// ! get the variable id
	public int id() {
		return id_;
	}

	// ! get the number of assignments the variable can take
	public int size() {
		return nasgs_;
	}

	// ! test equality between two varaibles
	public boolean lt(DiscreteVariable other) {
		return id_ < other.id();
	}

	// ! test equality between two varaibles
	public boolean eq(DiscreteVariable other) {
		return id_ == other.id();
	}

	// ! Test inequality between two variables
	public boolean neq(DiscreteVariable other) {
		return id_ != other.id();
	}

	// ! load the variable from an archive
	// void load(iarchive& arc) { arc >> id_ >> nasgs_; }
	// ! save the variable to an archive
	// void save(oarchive& arc) const { arc << id_ << nasgs_; }

	// ! The variable id
	private int id_ = 0;
	// ! The number of assignments the variable takes
	private int nasgs_ = 0;

	public String toString() {
		return (new java.lang.Integer(id_)).toString();
	}
}
