package stream.data.graph.factor;

import java.io.Serializable;
import java.util.Set;
import java.util.Vector;

public class DiscreteDomain implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7013356364705323935L;

	private int MAX_DIM;

	public int _num_vars;
	public DiscreteVariable[] _vars;

	// ! Make an empy domain
	public DiscreteDomain(int MAX_DIM) {
		this.MAX_DIM = MAX_DIM;
		this._vars = new DiscreteVariable[MAX_DIM];

		_num_vars = 0;
	}

	// ! Make an empy domain
	public DiscreteDomain(int MAX_DIM, DiscreteDomain other) {
		this.MAX_DIM = MAX_DIM;
		this._vars = new DiscreteVariable[MAX_DIM];

		System.arraycopy(other._vars, 0, _vars, 0, MAX_DIM);

		_num_vars = other._num_vars;
	}

	// ! Make a single variable DiscreteDomain
	public DiscreteDomain(int MAX_DIM, DiscreteVariable v1) {
		this.MAX_DIM = MAX_DIM;
		this._vars = new DiscreteVariable[MAX_DIM];

		_num_vars = 1;
		// ASSERT_LE(_num_vars, MAX_DIM);
		_vars[0] = v1;
	}

	// ! Make a two variable DiscreteDomain
	public DiscreteDomain(int MAX_DIM, DiscreteVariable v1, DiscreteVariable v2) {
		this.MAX_DIM = MAX_DIM;
		this._vars = new DiscreteVariable[MAX_DIM];

		_num_vars = 2;
		// ASSERT_LE(_num_vars, MAX_DIM);
		// assert(v1 != v2);
		if (v1.lt(v2)) {
			_vars[0] = v1;
			_vars[1] = v2;
		} else {
			_vars[0] = v2;
			_vars[1] = v1;
		}
	}

	// ! Make a three variable DiscreteDomain
	public DiscreteDomain(int MAX_DIM, DiscreteVariable v1,
			DiscreteVariable v2, DiscreteVariable v3) {
		this.MAX_DIM = MAX_DIM;
		this._vars = new DiscreteVariable[MAX_DIM];

		_num_vars = 3;
		// ASSERT_LE(_num_vars, MAX_DIM);
		// ASSERT_NE(v1, v2);
		// ASSERT_NE(v2, v3);
		// ASSERT_NE(v1, v3);

		if (v1.lt(v2) && v2.lt(v3)) {
			_vars[0] = v1;
			_vars[1] = v2;
			_vars[2] = v3;
		} else if (v1.lt(v3) && v3.lt(v2)) {
			_vars[0] = v1;
			_vars[1] = v3;
			_vars[2] = v2;
		} else if (v2.lt(v1) && v1.lt(v3)) {
			_vars[0] = v2;
			_vars[1] = v1;
			_vars[2] = v3;
		} else if (v2.lt(v3) && v3.lt(v1)) {
			_vars[0] = v2;
			_vars[1] = v3;
			_vars[2] = v1;
		} else if (v3.lt(v1) && v1.lt(v2)) {
			_vars[0] = v3;
			_vars[1] = v1;
			_vars[2] = v2;
		} else if (v3.lt(v1) && v1.lt(v2)) {
			_vars[0] = v3;
			_vars[1] = v1;
			_vars[2] = v2;
		} else {
			// throw("Invalid Case!");
		}
	}

	// ! Make a DiscreteDomain from a vector of variables
	public DiscreteDomain(int MAX_DIM, Vector<DiscreteVariable> variables) {
		this.MAX_DIM = MAX_DIM;
		this._vars = new DiscreteVariable[MAX_DIM];

		this._num_vars = variables.size();
		// ASSERT_LE(_num_vars, MAX_DIM);
		for (int i = 0; i < _num_vars; ++i)
			_vars[i] = variables.get(i);
		java.util.Arrays.sort(_vars, 0, _num_vars);
	}

	// ! Make a DiscreteDomain from a set of variables
	public DiscreteDomain(int MAX_DIM, Set<DiscreteVariable> variables) {
		this.MAX_DIM = MAX_DIM;
		this._vars = new DiscreteVariable[MAX_DIM];

		_num_vars = variables.size();
		// ASSERT_LE(_num_vars, MAX_DIM);
		int i = 0;
		// System.out.println("WARNING! FIX ME!");
		for (DiscreteVariable var : variables)
			_vars[i++] = var;
	}

	public DiscreteAssignment begin() {
		return new DiscreteAssignment(MAX_DIM, this);
	}

	public DiscreteAssignment end() {
		DiscreteAssignment ret = new DiscreteAssignment(MAX_DIM, this);
		ret.make_end();
		return ret;
	}

	public DiscreteDomain add(DiscreteVariable var) {
		if (_num_vars == 0) {
			_vars[_num_vars] = var;
			_num_vars++;
		} else if (_vars[_num_vars - 1].lt(var)) {
			_vars[_num_vars] = var;
			_num_vars++;
		} else
			add(new DiscreteDomain(MAX_DIM, var));

		return this;
	}

	// ! add the DiscreteDomain to this DiscreteDomain
	public DiscreteDomain add(DiscreteDomain other) {
		if (other.getNumVars() == 0)
			return this;
		DiscreteDomain backup = new DiscreteDomain(MAX_DIM, this);
		_num_vars = 0;
		for (int i = 0, j = 0; i < backup.getNumVars()
				|| j < other.getNumVars();) {
			// ASSERT_LE(_num_vars, MAX_DIM);
			// Both
			if (i < backup.getNumVars() && j < other.getNumVars()
					&& _num_vars < MAX_DIM) {
				if (backup.var(i).lt(other.var(j)))
					_vars[_num_vars++] = backup.var(i++);
				else if (other.var(j).lt(backup.var(i)))
					_vars[_num_vars++] = other.var(j++);
				else {
					_vars[_num_vars++] = backup.var(i++);
					j++;
				}
			} else if (i < backup.getNumVars() && _num_vars < MAX_DIM) {
				_vars[_num_vars++] = backup.var(i++);
			} else if (j < other.getNumVars() && _num_vars < MAX_DIM) {
				_vars[_num_vars++] = other.var(j++);
			} else {
				// Unreachable
				System.out.println("Unreachable case in domain operator+=");
			}
		}
		return this;
	}

	// ! add the other DiscreteDomain to this DiscreteDomain
	// DiscreteDomain operator+(const DiscreteDomain& other) const {
	// DiscreteDomain dom = *this;
	// return dom += other;
	// }

	// ! subtract the other DiscreteDomain from this DiscreteDomain
	public DiscreteDomain sub(DiscreteDomain other) {
		if (other.getNumVars() == 0)
			return this;

		int tmp_num_vars = 0;
		for (int i = 0, j = 0; i < _num_vars; ++i) {
			// advance the other index
			for (; j < other._num_vars && _vars[i].id() > other._vars[j].id(); ++j)
				;
			if (!(j < other._num_vars && _vars[i].id() == other._vars[j].id())) {
				_vars[tmp_num_vars++] = _vars[i];
			}
		}
		_num_vars = tmp_num_vars;
		return this;
	}

	// ! subtract the other DiscreteDomain from this DiscreteDomain
	// DiscreteDomain operator-(const DiscreteDomain& other) const {
	// DiscreteDomain dom = *this;
	// return dom -= other;
	// }

	public DiscreteDomain intersect(DiscreteDomain other) {
		DiscreteDomain new_dom = new DiscreteDomain(MAX_DIM);
		new_dom._num_vars = 0;
		for (int i = 0, j = 0; i < getNumVars() && j < other.getNumVars();) {
			if (_vars[i] == other._vars[j]) {
				// new DiscreteDomain gets the variable
				new_dom._vars[new_dom._num_vars] = _vars[i];
				// Everyone advances
				new_dom._num_vars++;
				i++;
				j++;
			} else {
				// otherwise increment one of the variables
				if (_vars[i].lt(other._vars[j]))
					i++;
				else
					j++;
			}
		}
		return new_dom;
	}

	// ! Get the number of variables
	public int getNumVars() {
		return _num_vars;
	}

	// ! Get the ith variable
	public DiscreteVariable var(int index) {
		// ASSERT_LT(index, _num_vars);
		return _vars[index];
	}

	/**
	 * get the index of the variable or returns number of variables if the index
	 * is not found
	 */
	public int varLocation(int var_id) {
		int location = _num_vars;
		for (int i = 0; i < _num_vars && !(location < _num_vars); ++i) {
			if (_vars[i].id() == var_id)
				location = i;
		}
		return location;
	}

	// ! determine the number of assignments
	public int size() {
		int sum = 0;
		if (_num_vars > 0) {
			sum = 1;
			for (int i = 0; i < _num_vars; ++i) {
				// Require variables to be sorted order
				// if(i > 0) ASSERT_LT( _vars[ i-1], _vars[i] );
				// and have positive arity
				// ASSERT_GT(_vars[i].size(), 0);
				sum *= _vars[i].size();
			}
		}
		return sum;
	}

	// ! test whether two DiscreteDomains are equal
	public boolean eq(DiscreteDomain other) {
		if (getNumVars() != other.getNumVars())
			return false;
		for (int i = 0; i < getNumVars(); ++i) {
			if (var(i) != other.var(i))
				return false;
		}
		return true;
	}

	// ! test whether two DiscreteDomains are not equal
	public boolean neq(DiscreteDomain other) {
		return !(this.eq(other));
	}

	// ! Get the first assignment in the DiscreteDomain
	// assignment_type begin() const;
	// ! Get the second assignment in the DiscreteDomain
	// assignment_type end() const;

	// void load(iarchive& arc) {
	// arc >> _num_vars;
	// ASSERT_LE(_num_vars, MAX_DIM);
	// for(int i = 0; i < _num_vars; ++i) arc >> _vars[i];
	// }

	// void save(oarchive& arc) const {
	// arc << _num_vars;
	// for(int i = 0; i < _num_vars; ++i) arc << _vars[i];
	// }

	public String toString() {
		String out = new String("{");
		for (int i = 0; i < getNumVars(); ++i) {
			out += var(i);
			if (i < getNumVars() - 1)
				out += ", ";
		}
		return out + "}";
	}
}
