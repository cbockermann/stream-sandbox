package stream.data.graph.factor;

import java.io.Serializable;
import java.util.Random;
import java.util.Vector;

public class DiscreteAssignment implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1224419532696679430L;

	private int MAX_DIM;

	private DiscreteDomain _args;
	public int[] _asgs; // init with size MAX_DIM
	private int _index;

	private static Random rng = new Random();

	public DiscreteAssignment(int MAX_DIM) {

		this.MAX_DIM = MAX_DIM;
		this._asgs = new int[MAX_DIM];
		this._args = new DiscreteDomain(MAX_DIM);
		this._index = 0;
	}

	public DiscreteAssignment(int MAX_DIM, DiscreteAssignment other) {

		this.MAX_DIM = MAX_DIM;
		this._asgs = new int[MAX_DIM];

		this._args = new DiscreteDomain(MAX_DIM, other.getArgs());
		System.arraycopy(other._asgs, 0, this._asgs, 0, MAX_DIM);
		this._index = other.getLinearIndex();
	}

	// ! Construct a zero discrete_assignment over the domain
	public DiscreteAssignment(int MAX_DIM, DiscreteDomain args) {
		this.MAX_DIM = MAX_DIM;
		this._asgs = new int[MAX_DIM];

		this._args = new DiscreteDomain(MAX_DIM, args);
		this._index = 0;

		for (int i = 0; i < args.getNumVars(); ++i) {
			this._asgs[i] = 0;
		}
	}

	// ! Construct a zero discrete_assignment over the domain
	public DiscreteAssignment(int MAX_DIM, DiscreteDomain args, int index) {
		this.MAX_DIM = MAX_DIM;
		this._asgs = new int[MAX_DIM];

		this._args = new DiscreteDomain(MAX_DIM, args);
		this._index = index;

		// boolean idxLTargsize = index < _args.size();
		// assert idxLTargsize;

		recompute_asgs();
	}

	// ! construct an discrete_assignment from one variable
	public DiscreteAssignment(int MAX_DIM, DiscreteVariable v1, int asg1) {
		this.MAX_DIM = MAX_DIM;
		this._asgs = new int[MAX_DIM];

		this._args = new DiscreteDomain(MAX_DIM, v1);
		this._index = asg1;

		// boolean asgLTsize = asg1 < v1.size();
		// assert asgLTsize;
		_asgs[0] = asg1;
	}

	// ! construct an discrete_assignment from two variables
	public DiscreteAssignment(int MAX_DIM, DiscreteVariable v1, int asg1,
			DiscreteVariable v2, int asg2) {
		this.MAX_DIM = MAX_DIM;
		this._asgs = new int[MAX_DIM];

		this._args = new DiscreteDomain(MAX_DIM, v1, v2);
		this._index = 0;

		setAsg(v1.id(), asg1);
		setAsg(v2.id(), asg2);
	}

	// ! Construct an discrete_assignment from a vector of variables and a
	// ! vector of values
	public DiscreteAssignment(int MAX_DIM, DiscreteDomain args,
			Vector<Integer> values) {
		this.MAX_DIM = MAX_DIM;
		this._asgs = new int[MAX_DIM];

		this._args = new DiscreteDomain(MAX_DIM, args);
		this._index = 0;

		for (int i = 0; i < _args.getNumVars(); ++i) {

			// boolean vLTsize = values[i] < args.var(i).size();
			// assert vLTsize;

			this._asgs[i] = values.get(i);
		}
		recomputeLinearIndex();
	}

	// ! Construct the union of two discrete_assignments
	public DiscreteAssignment union(DiscreteAssignment other) {
		DiscreteAssignment result = new DiscreteAssignment(MAX_DIM, getArgs()
				.add(other.getArgs()));

		int i = 0, j = 0, k = 0;
		while (i < getNumVars() && j < other.getNumVars()) {
			// extra increment if necessary
			// boolean kLTnumvars = k < result.getNumVars();
			// assert kLTnumvars;

			result._asgs[k] = (result.getArgs().var(k) == getArgs().var(i)) ? getAsgAt(i)
					: other.getAsgAt(j);

			// if the variables are the same then the discrete_assignments must
			// also be the same

			// boolean fancyAssert = !(getArgs().var(i) ==
			// other.getArgs().var(j)) || (getAsgAt(i) == other.getAsgAt(j));
			// assert fancyAssert;
			// move indexs
			i += ((getArgs().var(i) == result.getArgs().var(k)) ? 1 : 0);
			j += ((other.getArgs().var(j) == result.getArgs().var(k)) ? 1 : 0);
			k++;
		}
		while (i < getNumVars())
			result._asgs[k++] = getAsgAt(i++);
		while (j < other.getNumVars())
			result._asgs[k++] = other.getAsgAt(j++);
		// recompute the linear index of the result
		result.recomputeLinearIndex();
		return result;
	}

	// ! Get the variable in the discrete_assignment
	public DiscreteDomain getArgs() {
		return _args;
	}

	// ! get the number of variables
	public int getNumVars() {
		return _args.getNumVars();
	}

	// ! get the size of the discrete_assignment
	public int size() {
		return _args.size();
	}

	// ! Get the next discrete_assignment
	public DiscreteAssignment increment() {

		// boolean idxLTsize = _index < _args.size();
		// assert idxLTsize;

		// Increment the index
		++_index;

		// Update the discrete_assignments
		for (int i = 0; i < _args.getNumVars(); ++i) {
			_asgs[i] = ((_asgs[i] + 1) % _args.var(i).size());
			if (_asgs[i] > 0) {
				return this;
			}
		}

		// Reached end
		make_end();
		return this;
	}

	// ! Uniformly sample a new index value
	public void uniform_sample() {
		set_index(rng.nextInt(size()));
	}

	// ! Get the index of this discrete_assignment
	public int getLinearIndex() {
		return this._index;
	}

	public int asg(int var_id) {
		int index = this._args.varLocation(var_id);

		// boolean idxLTnvar = index < _args.getNumVars();
		// assert idxLTnvar;

		return _asgs[index];
	}

	public int getAsgAt(int index) {

		// boolean idxLTnvar = index < _args.getNumVars()
		// assert idxLTnvar;

		return _asgs[index];
	}

	public void setAsg(int var_id, int value) {
		int index = _args.varLocation(var_id);
		// assert(index < _args.getNumVars());
		// assert(value < _args.var(index).size());
		_asgs[index] = value;
		recomputeLinearIndex();
	}

	public void setAsgAt(int index, int value) {
		// assert(index < _args.getNumVars());
		// assert(value < _args.var(index).size());
		_asgs[index] = value;
		recomputeLinearIndex();
	}

	public void set_index(int index) {
		// assert(index < _args.size());
		_index = index;
		recompute_asgs();
	}

	// ! Tests whether two discrete_assignments are equal
	public boolean eq(DiscreteAssignment other) {
		return _index == other._index;
	}

	// ! Tests whether two discrete_assignments are not equal
	public boolean neq(DiscreteAssignment other) {
		return _index != other._index;
	}

	// ! Tests whether this discrete_assignment is < other
	public boolean lt(DiscreteAssignment other) {
		return _index < other._index;
	}

	// ! Make this an ending discrete_assignment
	public void make_end() {
		_index = Integer.MAX_VALUE;
		// for(size_t i = 0; i < _args.getNumVars(); ++i)
		// _asgs[i] = _args.var(i).size();
	}

	// ! Restrict the discrete_assignment to an discrete_assignment over the
	// subdomain
	public DiscreteAssignment restrict(DiscreteDomain sub_domain) {
		DiscreteAssignment other_asg = new DiscreteAssignment(MAX_DIM,
				sub_domain);
		int index = 0;
		// Map the variables
		for (int i = 0; i < _args.getNumVars()
				&& index < sub_domain.getNumVars(); ++i) {
			if (sub_domain.var(index) == _args.var(i)) {
				other_asg._asgs[index] = _asgs[i];
				index++;
			}
		}
		// assert(index == sub_domain.getNumVars());

		// Recompute the index
		other_asg.recomputeLinearIndex();
		return other_asg;
	} // end of restrict

	// ! Update the variables in this discrete_assignment with the values in the
	// ! other discrete_assignment
	public void update(DiscreteAssignment other) {
		for (int i = 0, j = 0; i < getNumVars() && j < other.getNumVars();) {
			if (_args.var(i) == other._args.var(j)) {
				_asgs[i] = other._asgs[j];
				i++;
				j++;
			}
			while (i < getNumVars() && _args.var(i).lt(other.getArgs().var(j)))
				i++;
			while (j < other.getNumVars()
					&& other.getArgs().var(j).lt(_args.var(i)))
				j++;
		}
		recomputeLinearIndex();
	}

	// void load(iarchive& arc) {
	// arc >> _args;
	// arc >> _index;
	// recompute_asgs();
	// }

	// void save(oarchive& arc) const {
	// arc << _args;
	// arc << _index;
	// }

	// ! Recompute the index from the discrete_assignment
	private void recomputeLinearIndex() {
		int multiple = 1;
		// Clear the index
		_index = 0;
		for (int i = 0; i < _args.getNumVars(); ++i) {
			_index += multiple * _asgs[i];
			multiple *= _args.var(i).size();
		}
	}

	// ! Recompute the discrete_assignments from the index
	private void recompute_asgs() {
		// assert(_index < _args.size());
		int quotient = _index;
		for (int i = 0; i < _args.getNumVars(); ++i) {
			_asgs[i] = quotient % _args.var(i).size();
			quotient /= _args.var(i).size();
		}
	}

	public String toString() {
		String out = new String("{");
		for (int i = 0; i < getArgs().getNumVars(); ++i) {
			out += "v_" + getArgs().var(i).id() + "=" + getAsgAt(i);
			if (i < getArgs().getNumVars() - 1)
				out += ", ";
		}
		out += "}=" + getLinearIndex();
		return out;
	}
}
