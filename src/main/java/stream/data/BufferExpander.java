package stream.data;

import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import stream.Data;
import stream.expressions.ExpressionResolver;
import stream.service.BufferService;

/**
 * @author Hendrik Blom
 * 
 */
public class BufferExpander extends DataExpander {

	static Logger log = LoggerFactory.getLogger(BufferExpander.class);

	private BufferService bufferService;
	private String key;

	public BufferService getBufferService() {
		return bufferService;
	}

	public void setBufferService(BufferService bufferService) {
		this.bufferService = bufferService;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	@Override
	public Data process(Data input) {
		Iterator<Data> iter = expand(input);
		if (iter == null) {
			log.error("Could not expand {}", key);
			return input;
		}
		while (iter.hasNext())
			iterate(iter);

		return input;
	}

	protected Iterator<Data> expand(Data input) {
		String datakey = ExpressionResolver.resolve(key, context, input)
				.toString();
		List<Data> dataList = bufferService.read(datakey);
		if (dataList == null)
			return null;
		return dataList.iterator();
	}
}
