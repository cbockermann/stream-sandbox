package stream.data;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import stream.ConditionedProcessor;
import stream.Data;
import stream.expressions.ExpressionResolver;
import stream.service.BufferService;

/**
 * @author Hendrik Blom
 * 
 */
public class RemoveFromBuffer extends ConditionedProcessor {

	static Logger log = LoggerFactory.getLogger(RemoveFromBuffer.class);

	private BufferService bufferService;
	private String key;

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public BufferService getBufferService() {
		return bufferService;
	}

	public void setBufferService(BufferService bufferservice) {
		this.bufferService = bufferservice;
	}

	@Override
	public Data processMatchingData(Data item) {
		if (bufferService == null) {
			log.error("No BufferService injected!");
			return item;
		}
		String localkey = (String) ExpressionResolver.resolve(key, context,
				item);
		bufferService.remove(localkey);
		return item;
	}
}
