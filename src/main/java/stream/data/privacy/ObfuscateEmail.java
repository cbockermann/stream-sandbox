/**
 * 
 */
package stream.data.privacy;

import java.io.Serializable;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import stream.Data;
import stream.ProcessorException;

/**
 * @author chris
 * 
 */
public class ObfuscateEmail extends AbstractObfuscator {

	Pattern pattern;

	public ObfuscateEmail() {
		super(new HashTable());
		setPattern("([A-Za-z0-9\\-_]+(@|%40)[\\-_A-Za-z0-9\\.]+)");
	}

	/**
	 * @return the pattern
	 */
	public String getPattern() {
		return pattern.toString();
	}

	/**
	 * @param pattern
	 *            the pattern to set
	 */
	private void setPattern(String pattern) {
		try {
			this.pattern = Pattern.compile(pattern);
		} catch (Exception e) {
			throw new ProcessorException(this, "Invalid regular expresion '"
					+ pattern + "': " + e.getMessage());
		}
	}

	/**
	 * @see stream.Processor#process(stream.data.Data)
	 */
	@Override
	public Data obfuscate(Data input) {

		for (String key : input.keySet()) {

			Serializable value = input.get(key);
			String string = value.toString();
			Matcher m = pattern.matcher(string);
			if (m.find()) {
				for (int g = 0; g < m.groupCount(); g++) {
					String grp = m.group(g);
					String hash = getHashCode(grp);
					String repl;
					if (string.indexOf("@") >= 0)
						repl = hash + "@pseudonym.com";
					else
						repl = hash + "%40pseudonym.com";
					log.trace("   replacing {} => {}", grp, repl);
					while (string.indexOf(grp) > 0) {
						string = string.replace(grp, repl);
					}
				}

				input.put(key, string);
			}
		}

		return input;
	}
}