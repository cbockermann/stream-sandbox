/**
 * 
 */
package stream.data.privacy;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import stream.ConditionedProcessor;
import stream.Data;

/**
 * @author chris
 * 
 */
public abstract class AbstractObfuscator extends ConditionedProcessor {

	static Logger log = LoggerFactory.getLogger(AbstractObfuscator.class);
	protected HashTableService tableService;
	protected final HashTable table;

	public AbstractObfuscator(HashTable hashTable) {
		this.table = hashTable;
	}

	public String getHashCode(String str) {

		if (tableService != null) {
			String val = tableService.get(str);
			log.debug("Service returned: {}", val);
			if (val != null)
				return val;
		}

		String code = table.getHashCode(str);
		if (tableService != null) {
			tableService.set(str, code);
		}

		return code;
	}

	public void setHashTable(HashTableService service) {
		log.debug("Service injection! service = {}", service);
		this.tableService = service;
	}

	/**
	 * @see stream.ConditionedProcessor#processMatchingData(stream.Data)
	 */
	public Data processMatchingData(Data item) {
		return obfuscate(item);
	}

	public abstract Data obfuscate(Data data);
}
