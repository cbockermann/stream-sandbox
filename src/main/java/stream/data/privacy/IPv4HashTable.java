/**
 * 
 */
package stream.data.privacy;

/**
 * @author chris
 * 
 */
public class IPv4HashTable extends HashTable {

	public String hashOctet(String octet) {
		String octetHash = super.computeHash(octet).substring(0, 7);
		Long lng = Long.parseLong(octetHash, 16);
		return Long.toString(1 + (lng % 254));
	}

	/**
	 * @see stream.data.privacy.HashTable#computeHash(java.lang.String)
	 */
	@Override
	public String computeHash(String str) {
		StringBuffer s = new StringBuffer();
		String[] octets = str.split("\\.");
		for (int i = 0; i < octets.length; i++) {
			String octet = octets[i];
			s.append(hashOctet(octet));
			if (i + 1 < octets.length)
				s.append(".");
		}
		return s.toString();
	}
}
