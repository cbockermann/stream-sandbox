/**
 * 
 */
package stream.data.privacy;

import java.io.Serializable;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import stream.Data;

/**
 * @author chris
 * 
 */
public class IPv4Obfuscator extends AbstractObfuscator {

	static Logger log = LoggerFactory.getLogger(IPv4Obfuscator.class);

	final Pattern pattern = Pattern
			.compile("(\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3})");

	public IPv4Obfuscator() {
		super(new IPv4HashTable());
	}

	/**
	 * @see stream.Processor#process(stream.Data)
	 */
	@Override
	public Data obfuscate(Data input) {

		try {
			for (String key : input.keySet()) {

				Serializable value = input.get(key);
				String string = value.toString();
				int start = 0;
				Matcher m = pattern.matcher(string);
				while (m.find(start)) {
					start = m.start();
					String grp = string.substring(start, m.end());
					String repl = this.getHashCode(grp);
					string = string.replace(grp, repl);
					m = pattern.matcher(string);
					start += repl.length();
				}
				input.put(key, string);
			}
		} catch (Exception e) {
			log.error("Failed to obfuscate: {}", e.getMessage());
			return null;
		}
		return input;
	}
}