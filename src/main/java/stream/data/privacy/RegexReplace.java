/**
 * 
 */
package stream.data.privacy;

import java.io.Serializable;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import stream.ProcessorException;
import stream.annotations.Parameter;
import stream.Data;

/**
 * @author chris
 * 
 */
public class RegexReplace extends AbstractObfuscator {

	Pattern pattern;

	public RegexReplace() {
		super(new HashTable());
	}

	/**
	 * @return the pattern
	 */
	public String getPattern() {
		return pattern.toString();
	}

	/**
	 * @param pattern
	 *            the pattern to set
	 */
	@Parameter(required = true, description = "A regular expression that contains groups for replacement.")
	public void setPattern(String pattern) {
		try {
			this.pattern = Pattern.compile(pattern);
		} catch (Exception e) {
			throw new ProcessorException(this, "Invalid regular expresion '"
					+ pattern + "': " + e.getMessage());
		}
	}

	/**
	 * @see stream.Processor#process(stream.Data)
	 */
	@Override
	public Data obfuscate(Data input) {

		for (String key : input.keySet()) {

			Serializable value = input.get(key);
			String string = value.toString();
			if (string.indexOf("%40") > 0) {
				log.info("Found '@'");
			}
			Matcher m = pattern.matcher(string);
			if (m.find()) {
				log.info("Found pattern {}!", pattern);
				for (int g = 0; g < m.groupCount(); g++) {
					String grp = m.group(g);
					String repl = this.getHashCode(grp) + "@pseudonym.com";
					log.info("   replacing {} => {}", grp, repl);
					while (string.indexOf(grp) > 0) {
						string = string.replace(grp, repl);
					}
				}

				input.put(key, string);
			}
		}

		return input;
	}
}