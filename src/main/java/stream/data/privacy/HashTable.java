/**
 * 
 */
package stream.data.privacy;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import stream.util.MD5;

/**
 * @author chris
 * 
 */
public class HashTable implements HashTableService {

	final Map<String, String> table = new HashMap<String, String>();
	final Double rnd;

	public HashTable() {
		this(System.currentTimeMillis());
	}

	public HashTable(long seed) {
		rnd = (new Random(seed)).nextDouble();
	}

	public final String getHashCode(String str) {
		if (table.containsKey(str))
			return table.get(str);

		String code = computeHash(str);
		table.put(str, code);
		return code;
	}

	public String computeHash(String str) {
		return MD5.md5(rnd + str);
	}

	/**
	 * @see stream.service.Service#reset()
	 */
	@Override
	public void reset() throws Exception {
		table.clear();
	}

	/**
	 * @see stream.data.privacy.HashTableService#get(java.lang.String)
	 */
	@Override
	public String get(String key) {
		return table.get(key);
	}

	/**
	 * @see stream.data.privacy.HashTableService#set(java.lang.String,
	 *      java.lang.String)
	 */
	@Override
	public void set(String key, String value) {
		table.put(key, value);
	}
}
