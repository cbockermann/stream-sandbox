/**
 * 
 */
package stream.data.privacy;

import stream.service.Service;

/**
 * @author chris
 * 
 */
public interface HashTableService extends Service {

	public String get(String key);

	public void set(String key, String value);

}
