/**
 * 
 */
package stream.data.privacy;

import java.io.Serializable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import stream.Data;
import stream.ProcessorException;
import stream.annotations.Description;
import stream.annotations.Parameter;
import stream.data.DataFactory;

/**
 * <p>
 * This class obfuscates all occurences of a values found in the specified
 * attributes from the attributes and from all other attribute values (string
 * values). The values are replaced with a hashed value of the value to
 * obfuscate.
 * </p>
 * 
 * @author Christian Bockermann &lt;christian.bockermann@udo.edu&gt;
 * 
 */
@Description(group = "Data Stream.Processing.Transformations.Privacy")
public class Obfuscator extends AbstractObfuscator {

	static Logger log = LoggerFactory.getLogger(Obfuscator.class);
	String[] keys;

	public Obfuscator() {
		this(new HashTable());
		this.table.set("-", "-");
	}

	public Obfuscator(HashTable hashTable) {
		super(hashTable);
	}

	/**
	 * @return the keys
	 */
	public String[] getKeys() {
		return keys;
	}

	/**
	 * @param keys
	 *            the keys to set
	 */
	@Parameter(required = true, description = "The keys (attributes) whose's values are to be pseudonymized.")
	public void setKeys(String[] keys) {
		this.keys = keys;
	}

	/**
	 * @see stream.ConditionedProcessor#processMatchingData(stream.Data)
	 */
	@Override
	public Data obfuscate(Data input) {

		if (keys == null) {
			log.info("No keys specified, not obfuscating item");
			return input;
		}

		try {
			Data obfuscated = DataFactory.create();

			for (String key : keys) {
				Serializable value = input.get(key);
				if (value == null) {
					continue;
				}

				if (value instanceof String
						|| Number.class.isAssignableFrom(value.getClass())) {

					String replace = value.toString();
					String replacement = getHashCode(replace);
					if (replace.length() < replacement.length()) {
						String repl = replacement.substring(0, 8);
						table.set(replace, repl);
						replacement = repl;
					}

					log.debug("obfuscate {} => {}", replace, replacement);

					for (String k : input.keySet()) {

						if (replace.equalsIgnoreCase(replacement)) {
							obfuscated.put(k, input.get(k));
							continue;
						}

						String val = input.get(k).toString();
						if (val.indexOf(replace) >= 0) {
							while (val.indexOf(replace) >= 0) {
								val = val.replace(replace, replacement);
							}
							obfuscated.put(k, val);
						} else {
							obfuscated.put(k, input.get(k));
						}
					}

				} else {
					log.error("Don't know how to obfuscate value of type {}!",
							value.getClass());
					throw new ProcessorException(this,
							"Don't know how to obfuscate value of type "
									+ value.getClass() + "!");
				}

			}
			return obfuscated;

		} catch (Exception e) {
			log.error("Failed to obfuscate item: {}", e.getMessage());
			return null;
		}
	}
}