/**
 * 
 */
package stream.data.privacy;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.LinkedHashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import stream.annotations.Parameter;
import stream.Data;
import stream.io.AbstractSQLProcessor;

/**
 * @author chris
 * 
 */
public class DatabaseHashTable extends AbstractSQLProcessor implements
		HashTableService {

	static Logger log = LoggerFactory.getLogger(DatabaseHashTable.class);
	String table = "HASH_TABLE";
	boolean tableExists = false;

	/**
	 * @return the table
	 */
	public String getTable() {
		return table;
	}

	/**
	 * @param table
	 *            the table to set
	 */
	@Parameter(required = true, description = "The name of the database table to store the (key,value) pairs.")
	public void setTable(String table) {
		this.table = table;
	}

	/**
	 * @see stream.service.Service#reset()
	 */
	@Override
	public void reset() throws Exception {
		Connection con = openConnection();
		try {

			if (con != null) {
				Statement drop = con.createStatement();
				drop.executeUpdate("DROP TABLE " + getTable());
				drop.close();
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		close(con);
	}

	/**
	 * @see stream.data.privacy.HashTableService#get(java.lang.String)
	 */
	@Override
	public String get(String key) {

		Connection con = openConnection();
		String value = null;

		try {

			if (!tableExists) {
				Map<String, Class<?>> cols = new LinkedHashMap<String, Class<?>>();
				cols.put("key", String.class);
				cols.put("value", String.class);
				String create = dialect.getCreateTableCommand(table, cols);
				log.debug("Create table statement: {}", create);
				PreparedStatement p = con.prepareStatement(create);
				int rc = p.executeUpdate();
				log.debug("Running {} returned: {}", create, rc);
				p.close();
				tableExists = true;
			}

			PreparedStatement stmt = con.prepareStatement("SELECT value FROM "
					+ table + " WHERE `key` = ?");
			stmt.setString(1, key);
			log.debug("select: {}", stmt);

			ResultSet rs = stmt.executeQuery();
			if (rs.next()) {
				value = rs.getString(1);
			}
			rs.close();
			stmt.close();

		} catch (Exception e) {
			log.error("Failed to run select: {}", e.getMessage());
			if (log.isDebugEnabled())
				e.printStackTrace();
		}

		close(con);
		return value;
	}

	/**
	 * @see stream.data.privacy.HashTableService#set(java.lang.String,
	 *      java.lang.String)
	 */
	@Override
	public void set(String key, String value) {

		Connection con = openConnection();
		try {
			PreparedStatement stmt = con.prepareStatement("INSERT INTO "
					+ table + " (" + dialect.mapColumnName("key") + ","
					+ dialect.mapColumnName("value") + ") VALUES ( ?, ? )");
			stmt.setString(1, key);
			stmt.setString(2, value);

			int rc = stmt.executeUpdate();
			log.info("Insert returned: {}", rc);
		} catch (Exception e) {
			log.error("Failed to insert into table: {}", e.getMessage());
		}
		close(con);
	}

	/**
	 * @see stream.Processor#process(stream.Data)
	 */
	@Override
	public Data process(Data input) {
		return input;
	}
}
