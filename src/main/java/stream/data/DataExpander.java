package stream.data;

import java.util.Iterator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import stream.Data;
import stream.ProcessorList;

/**
 * @author Hendrik Blom
 * 
 */
public abstract class DataExpander extends ProcessorList {

	static Logger log = LoggerFactory.getLogger(DataExpander.class);

	@Override
	public Data process(Data input) {
		Iterator<Data> iter = null;
		try {
			iter = expand(input);
		} catch (Exception e) {
			log.error("Could not expand {}", input);
			e.printStackTrace();
		}
		if (iter == null)
			return input;
		iterate(iter);
		return input;
	}

	protected void iterate(Iterator<Data> iter) {
		while (iter.hasNext())
			super.process(iter.next());
	}

	protected abstract Iterator<Data> expand(Data input) throws Exception;

}
