package stream.data;

import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import stream.Data;
import stream.io.CsvStream;

/**
 * @author Hendrik Blom
 * 
 */
/**
 * @author Hendrik Blom
 * 
 */
public class CsvExpander extends DataExpander {

	private String key;
	private String separator;
	private CsvStream csvStream;

	public String getSeparator() {
		return separator;
	}

	public void setSeparator(String separator) {
		this.separator = separator;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	@Override
	protected Iterator<Data> expand(Data input) throws Exception {
		URL url = (URL) input.get(key);
		if (url == null)
			return null;
		this.csvStream = new CsvStream(url, separator);
		csvStream.init();
		List<Data> dataList = new ArrayList<Data>();
		Data data = null;
		do {
			data = csvStream.readNext();
			dataList.add(data);
		} while (data != null);

		return dataList.iterator();
	}
}
