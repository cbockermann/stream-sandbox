/**
 * 
 */
package stream.data;

import net.minidev.json.JSONObject;
import stream.Data;
import stream.Processor;

/**
 * @author chris
 * @deprecated This class is now part of the stream-core package
 */
public class AsJSON implements Processor {

	String key = "@json";

	/**
	 * @return the key
	 */
	public String getKey() {
		return key;
	}

	/**
	 * @param key
	 *            the key to set
	 */
	public void setKey(String key) {
		this.key = key;
	}

	/**
	 * @see stream.Processor#process(stream.Data)
	 */
	@Override
	public Data process(Data input) {
		String json = JSONObject.toJSONString(input);
		input.put(key, json);
		return input;
	}
}
