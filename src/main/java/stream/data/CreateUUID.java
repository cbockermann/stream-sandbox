package stream.data;

import java.util.UUID;

import stream.AbstractProcessor;
import stream.Data;

public class CreateUUID extends AbstractProcessor {

	private String key;

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	@Override
	public Data process(Data input) {
		UUID uuid = UUID.randomUUID();
		input.put(key, uuid);
		return input;
	}
}
