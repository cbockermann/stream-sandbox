/*
 *  streams library
 *
 *  Copyright (C) 2011-2012 by Christian Bockermann, Hendrik Blom
 * 
 *  streams is a library, API and runtime environment for processing high
 *  volume data streams. It is composed of three submodules "stream-api",
 *  "stream-core" and "stream-runtime".
 *
 *  The streams library (and its submodules) is free software: you can 
 *  redistribute it and/or modify it under the terms of the 
 *  GNU Affero General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any 
 *  later version.
 *
 *  The stream.ai library (and its submodules) is distributed in the hope
 *  that it will be useful, but WITHOUT ANY WARRANTY; without even the implied 
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package stream.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import stream.ConditionedProcessor;
import stream.Context;
import stream.Data;
import stream.annotations.Description;
import stream.annotations.Parameter;
import stream.expressions.ExpressionResolver;

/**
 * <p>
 * This class implements a processor to set a value within a data item.
 * </p>
 * 
 * @author Christian Bockermann &lt;christian.bockermann@udo.edu&gt;
 * 
 */
@Description(group = "Data Stream.Processing.Transformations.Data")
public class RegExp extends ConditionedProcessor {
	protected String key;
	protected String value;
	protected String regExp;
	protected List<String> scope;
	private Pattern pattern;

	public RegExp() {
		super();
		scope = new ArrayList<String>();
	}

	/**
     * 
     */
	@Override
	public Data processMatchingData(Data data) {
		Object o = ExpressionResolver.resolve(value, context, data);
		if (key != null && o != null) {
			Serializable val = readValue(o.toString());
			if (scope.contains(Context.DATA_CONTEXT_NAME) || scope.isEmpty())
				data.put(key, val);
			if (scope.contains(Context.PROCESS_CONTEXT_NAME))
				context.set(key, val);
		}

		return data;
	}

	private Serializable readValue(String value) {
		Matcher m = pattern.matcher(value);
		if (m.find())
			return m.group();
		return null;
	}

	/**
	 * @return the key
	 */
	public String getKey() {
		return key;
	}

	/**
	 * @param key
	 *            the key to set
	 */
	public void setKey(String key) {
		this.key = key;
	}

	/**
	 * @return the value
	 */
	public String getValue() {
		return value;
	}

	/**
	 * @param value
	 *            the value to set
	 */
	public void setValue(String value) {
		this.value = value;
	}

	/**
	 * @return the scope
	 */
	public String[] getScope() {
		return scope.toArray(new String[scope.size()]);
	}

	/**
	 * @param scope
	 *            scope=data and scope=process
	 */
	@Parameter(defaultValue = Context.DATA_CONTEXT_NAME, required = false)
	public void setScope(String[] scope) {
		this.scope = Arrays.asList(scope);
	}

	public String getRegExp() {
		return regExp;
	}

	public void setRegExp(String regExp) {
		this.regExp = regExp;
		pattern = Pattern.compile(regExp);
	}
}