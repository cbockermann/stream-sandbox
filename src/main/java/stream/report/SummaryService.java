/**
 * 
 */
package stream.report;

import java.util.List;

import stream.data.Statistics;
import stream.service.Service;

/**
 * @author chris
 * 
 */
public interface SummaryService extends Service {

	public List<Statistics> getSummary();
}
