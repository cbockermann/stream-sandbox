/**
 * 
 */
package stream.report;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import stream.AbstractProcessor;
import stream.Data;
import stream.data.Statistics;
import stream.expressions.ExpressionResolver;

/**
 * @author chris
 * 
 */
public class Summary extends AbstractProcessor implements SummaryService {

	String aspect = "COUNT";
	String groupBy = "";

	Map<String, Statistics> summary = new LinkedHashMap<String, Statistics>();

	/**
	 * @return the aspect
	 */
	public String getAspect() {
		return aspect;
	}

	/**
	 * @param aspect
	 *            the aspect to set
	 */
	public void setAspect(String aspect) {
		this.aspect = aspect;
	}

	/**
	 * @return the groupBy
	 */
	public String getGroupBy() {
		return groupBy;
	}

	/**
	 * @param groupBy
	 *            the groupBy to set
	 */
	public void setGroupBy(String groupBy) {
		this.groupBy = groupBy;
	}

	/**
	 * @see stream.Processor#process(stream.Data)
	 */
	@Override
	public Data process(Data input) {

		String value = ExpressionResolver.expand(groupBy, context, input);
		synchronized (summary) {

			Statistics st = summary.get(value);
			if (st == null) {
				st = new Statistics();
				summary.put(value, st);
			}

			if ("COUNT".equals(aspect)) {
				st.add("count", 1.0d);
			} else {
				Serializable asp = input.get("aspect");
				if (asp != null) {
					st.add(asp.toString(), 1.0d);
				}
			}
		}

		return input;
	}

	/**
	 * @see stream.service.Service#reset()
	 */
	@Override
	public void reset() throws Exception {
		synchronized (summary) {
			summary.clear();
		}
	}

	/**
	 * @see stream.report.SumaryService#getSummary()
	 */
	@Override
	public List<Statistics> getSummary() {
		synchronized (summary) {

			List<Statistics> list = new ArrayList<Statistics>();
			for (String key : summary.keySet()) {
				Statistics st = new Statistics(summary.get(key));
				list.add(st);
			}
			return list;
		}
	}
}
