package stream.flow;

import stream.AbstractProcessor;
import stream.Data;

public class SetNull extends AbstractProcessor {

	private NullService nullService;

	public NullService getService() {
		return nullService;
	}

	public void setService(NullService nullService) {
		this.nullService = nullService;
	}

	@Override
	public Data process(Data input) {
		this.nullService.setNull(true);
		return input;
	}

}
