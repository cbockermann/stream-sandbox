package stream.flow;

import stream.AbstractProcessor;
import stream.ProcessContext;
import stream.Data;

public class Null extends AbstractProcessor implements NullService {

	private Boolean initNull;
	private Boolean isNull;

	public Null() {
		initNull = true;
	}

	public Boolean getInitNull() {
		return initNull;
	}

	public void setInitNull(Boolean initNull) {
		this.initNull = initNull;
	}

	@Override
	public void init(ProcessContext ctx) throws Exception {
		super.init(ctx);
		if (initNull)
			isNull = true;
		else
			isNull = false;
	}

	@Override
	public Data process(Data input) {
		if (isNull)
			return null;
		else
			return input;
	}

	@Override
	public void setNull(boolean isNull) {
		this.isNull = isNull;

	}

	@Override
	public void reset() throws Exception {
		setNull(false);
	}

}
