package stream.flow;

import stream.service.Service;

public interface NullService extends Service {

	public void setNull(boolean isNull);
}
