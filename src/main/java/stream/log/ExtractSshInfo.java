/**
 * 
 */
package stream.log;

import java.io.Serializable;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import stream.Data;
import stream.Processor;

/**
 * @author chris
 * 
 */
public class ExtractSshInfo implements Processor {

	static Logger log = LoggerFactory.getLogger(ExtractSshInfo.class);
	String from = "msg";

	/**
	 * @see stream.Processor#process(stream.Data)
	 */
	@Override
	public Data process(Data input) {

		Serializable val = input.get(from);
		if (val != null) {
			String value = val.toString();
			log.debug("Extracting ssh-info from message '{}'", value);

			Pattern fail = Pattern
					.compile("Failed password for (.*) from (.*) port (\\d+) ssh2");
			Matcher m = fail.matcher(value);
			if (m.find()) {
				input.put("ssh:user", m.group(1));
				input.put("ssh:remote_address", m.group(2));
				input.put("ssh:remote_port", m.group(3));
				input.put("ssh:status", "failed-password");
				return input;
			}

			Pattern ip = Pattern.compile("(\\d+\\.\\d+\\.\\d+\\.\\d+)");
			m = ip.matcher(value);

			if (m.find()) {
				log.debug("Found 'remote_address' in ssh message: {}",
						m.group());
				input.put("ssh:remote_address", m.group());
			}

			Pattern user = Pattern.compile("for user (\\w+)");
			m = user.matcher(value);
			if (m.find()) {
				input.put("ssh:user", m.group());
				for (int g = 0; g <= m.groupCount(); g++) {
					log.info("group {}: '{}'", g, m.group(g));
				}
				input.put("ssh:user", m.group(1));
				log.debug("Found 'user' in ssh message: {}", m.group(1));
			}

			// Failed password for invalid user junior from 60.32.179.108 port
			// 59223 ssh2
			Pattern inv = Pattern
					.compile("Failed password for invalid user (.*) from (.*) port (\\d+) ssh2");
			m = inv.matcher(value);
			if (m.find()) {
				input.put("ssh:user", m.group(1));
				input.put("ssh:remote_address", m.group(2));
				input.put("ssh:remote_port", m.group(3));
			}
		}

		return input;
	}
}
