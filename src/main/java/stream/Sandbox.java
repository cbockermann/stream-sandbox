/**
 * 
 */
package stream;

import java.io.File;
import java.lang.reflect.Method;

import stream.runtime.ProcessContainer;

/**
 * @author chris
 * 
 */
public class Sandbox {

	public static void setupLogging() {
		File logProp = new File("log4j.properties");
		if (logProp.canRead()) {

			try {
				Class<?> configurator = Class
						.forName("org.apache.log4j.PropertyConfigurator");
				Method configure = configurator.getMethod("configure",
						String.class);
				configure.invoke(null, logProp.getAbsolutePath());
			} catch (Exception e) {
				System.err
						.println("Failed to setup logging with log4j.properties: "
								+ e.getMessage());
			}
		}
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		if (args.length == 0) {
			System.out.println();
			System.out.println("Usage:");
			System.out
					.println("\tjava -jar stream-sandbox.jar path/to/container.xml");
			System.out.println();
			System.exit(-1);
		}

		setupLogging();

		File file = new File(args[0]);
		if (!file.exists()) {
			System.err.println("Cannot find file " + file.getAbsolutePath()
					+ " !");
			System.exit(-1);
		} else {
			try {
				ProcessContainer container = new ProcessContainer(file.toURI()
						.toURL());
				container.run();
			} catch (Exception e) {
				System.err.println("Error: " + e.getMessage());
				System.exit(-1);
			}
		}
	}
}
