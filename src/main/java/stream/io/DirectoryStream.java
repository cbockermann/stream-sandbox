/**
 * 
 */
package stream.io;

import java.io.File;
import java.net.URI;
import java.net.URL;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import stream.annotations.Description;
import stream.Data;

/**
 * @author Hendrik Blom
 * 
 */
@Description(group = "Data Stream.Sources")
public class DirectoryStream extends AbstractDataStream {

	Logger log = LoggerFactory.getLogger(DirectoryStream.class);

	private File dir;
	private String[] files;
	private String dirPath;
	private int count;

	public DirectoryStream(URL url) throws Exception {
		super(url);
		this.initReader();

	}

	@Override
	protected void initReader() throws Exception {
		dir = new File(new URI(url.toString()));
		if (!dir.isDirectory())
			throw new IllegalArgumentException("Directory not found");
		dirPath = dir.getAbsolutePath();
		files = dir.list();
		count = 0;
	}

	@Override
	public void close() {
	}

	@Override
	public void readHeader() throws Exception {
	}

	@Override
	public Data readItem(Data instance) throws Exception {
		if (count < files.length) {
			instance.put(
					"@url",
					new URL("file:" + dirPath
							+ System.getProperty("file.separator")
							+ files[count]));
			instance.put("@directory", dirPath);
			instance.put("@filename", files[count]);
			count++;
			return instance;
		}
		return null;
	}
}