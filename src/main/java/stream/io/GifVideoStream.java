/**
 * 
 */
package stream.io;

import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.net.URL;
import java.util.zip.GZIPInputStream;

import javax.imageio.ImageIO;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import stream.Data;
import stream.image.DisplayImage;

/**
 * @author chris
 * 
 */
public class GifVideoStream extends AbstractDataStream {

	static Logger log = LoggerFactory.getLogger(GifVideoStream.class);
	byte[] GIF_SIGNATURE = new byte[] { 0x47, 0x49, 0x46, 0x38 };

	int chunkSize = 1;
	URL url;
	BufferedInputStream input;
	byte[] buffer = new byte[2048 * 1024];
	int start = 0;
	long frameId;
	int limit = 0;
	long offset = 0L;

	public GifVideoStream(URL url) throws Exception {
		this(url.openStream());
		this.url = url;
	}

	public GifVideoStream(InputStream in) throws Exception {
		this.input = new BufferedInputStream(in);
	}

	/**
	 * @see stream.io.DataStream#close()
	 */
	@Override
	public void close() throws Exception {
		input.close();
	}

	/**
	 * @see stream.io.AbstractDataStream#readHeader()
	 */
	@Override
	public void readHeader() throws Exception {
	}

	/**
	 * @see stream.io.AbstractDataStream#readItem(stream.data.Data)
	 */
	@Override
	public synchronized Data readItem(Data instance) throws Exception {

		log.info("Reading FRAME-" + frameId);
		int read = input.read(buffer, limit, buffer.length - limit);
		if (read > 0)
			limit += read;
		log.info("Read {} bytes from input stream.", read);
		log.info("   buffer.limit is {}", limit);

		start = 0;
		int idx = findBytes(buffer, start, GIF_SIGNATURE);
		if (idx != 0) {
			log.error(
					"Error! Expecting stream to start with GIF_SIGNATURE! Found index {}",
					idx);
			byte[] begin = new byte[32];
			for (int i = 0; i < begin.length && i < buffer.length; i++) {
				begin[i] = buffer[i];
			}
			log.error("   byte prefix was: {}", begin);
			return null;
		}
		// log.info("found starting GIF sig at {}", idx);
		// log.info("frame starts at {} as follows: {}", idx,
		// getHex(getFirst(buffer, idx, 16)));

		int end = -1;

		do {
			end = findBytes(buffer, idx + GIF_SIGNATURE.length, GIF_SIGNATURE);
			// log.info("   detected new GIF at {}", end);
			if (end < 0) {
				// log.info("Trying to read more bytes...");
				while (limit < buffer.length) {
					int b = input.read();
					if (b > 0) {
						buffer[limit++] = (byte) b;
					} else
						break;
				}
				// log.info("   read next {} bytes", read);
				end = findBytes(buffer, idx + GIF_SIGNATURE.length,
						GIF_SIGNATURE);
				// log.info("check for next GIF returned: {}", end);
			}
		} while (end < 0 && read > 0);

		// log.info("frame ranges from {} to {}", start, end - 1);
		/*
		 * log.info(" frame starts at {}: {}", start, getHex(getFirst(buffer,
		 * start, 16))); log.info(" next frame starts at {}: {}", end,
		 * getHex(getFirst(buffer, end, 16)));
		 */

		byte[] img = new byte[end];
		for (int k = 0; k < img.length; k++) {
			img[k] = buffer[k];
			buffer[k] = buffer[k + end];
		}
		for (int i = end + 1; i < limit - end; i++) {
			buffer[i] = buffer[i + end];
		}
		limit -= end;
		// log.info("img frame starts as {}", getHex(getFirst(img, 0, 16)));
		// log.info("Beginning of remaining buffer is {}",
		// getHex(getFirst(buffer, 16)));
		start = 0;
		log.info("## Frame offset is {}   decimal: {}",
				Long.toHexString(offset), offset);
		offset += end;
		instance.put("frame:id", frameId++);
		instance.put("frame:data", img);
		// start = end + GIF_SIGNATURE.length;
		return instance;
	}

	public static int findBytes(byte[] buf, int from, byte[] sig) {

		for (int i = from; i + sig.length < buf.length; i++) {

			if (checkBytes(buf, i, sig))
				return i;
		}

		return -1;
	}

	public static String getHex(byte[] bytes) {
		StringBuffer s = new StringBuffer("[");
		for (int i = 0; i < bytes.length; i++) {
			s.append(Integer.toHexString((int) bytes[i]));
			if (i + 1 < bytes.length)
				s.append(", ");
		}
		s.append("]");
		return s.toString();
	}

	private static byte[] getFirst(byte[] buffer, int len) {
		return getFirst(buffer, 0, len);
	}

	private static byte[] getFirst(byte[] buffer, int off, int len) {
		byte[] begin = new byte[len];
		for (int i = off; i < begin.length && i < buffer.length; i++) {
			begin[i] = buffer[i];
		}
		return begin;
	}

	private static boolean checkBytes(byte[] buf, int pos, byte[] sig) {

		if (pos + sig.length < buf.length) {

			for (int p = 0; p < sig.length; p++) {

				if (buf[pos + p] != sig[p]) {
					return false;
				}
			}
		}

		log.info("Found signature {} at position {}", new String(sig), pos);
		return true;
	}

	public static void main(String[] args) throws Exception {

		URL url = new URL("file:/Volumes/RamDisk/test.raw.gz");
		InputStream in = new GZIPInputStream(url.openStream());

		GifVideoStream stream = new GifVideoStream(in);

		DisplayImage display = new DisplayImage();

		// stream.setLimit(300L);
		Data item = stream.readNext();
		while (item != null) {
			byte[] image = (byte[]) item.get("frame:data");
			if (image == null) {
				log.info("no image returned.");
				item = stream.readNext();
				continue;
			}
			log.info("Read frame {}, size {}", item.get("frame:id"),
					image.length);

			Long frame = new Long(item.get("frame:id") + "");
			if (frame % 3 == 1 || frame % 3 == 2) {

			} else {
				try {
					final BufferedImage img = ImageIO
							.read(new ByteArrayInputStream(image));
					// log.info("frame has size {} x {}", img.getHeight(),
					// img.getWidth());
					display.process(item);
				} catch (Exception e) {
					e.printStackTrace();
				}
				// System.in.read();
			}
			item = stream.readNext();
		}
	}

}