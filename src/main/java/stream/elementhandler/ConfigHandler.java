package stream.elementhandler;

import java.io.FileNotFoundException;
import java.net.URL;
import java.util.Iterator;

import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.w3c.dom.Element;

import stream.runtime.ContainerContext;
import stream.runtime.ElementHandler;
import stream.runtime.ProcessContainer;

public class ConfigHandler implements ElementHandler {

	@Override
	public void handleElement(ProcessContainer container, Element element)
			throws Exception {
		ContainerContext context = container.getContext();

		String urlString = element.getAttribute("file");
		URL url = null;
		if (urlString != null || !urlString.isEmpty())
			url = ConfigHandler.class.getResource(urlString);
		if (url != null) {
			Configuration config = new PropertiesConfiguration(url);
			Iterator<String> iter = config.getKeys();
			while (iter.hasNext()) {
				String key = iter.next();
				context.setProperty(key, config.getString(key));
			}
		} else
			throw new FileNotFoundException("File " + urlString
					+ "  not found!");
	}

	@Override
	public boolean handlesElement(Element element) {
		if (element == null)
			return false;

		return "config".equalsIgnoreCase(element.getNodeName());
	}

	@Override
	public String getKey() {
		return "Config";
	}

}
