package stream.plotter;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.PrintStream;
import java.net.URL;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import stream.AbstractProcessor;
import stream.ProcessContext;
import stream.Data;
import stream.expressions.ExpressionResolver;

/**
 * @author chris, Hendrik Blom
 * 
 */
public class GnuPlot extends AbstractProcessor {

	static Logger log = LoggerFactory.getLogger(GnuPlot.class);

	static String[] paths = new String[] { "/bin", "/usr/bin",
			"/usr/local/bin", "/sw/bin" };

	String url;

	String gnuplot;

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	@Override
	public void init(ProcessContext ctx) throws Exception {
		super.init(ctx);
		this.gnuplot = findGnuplot();
		if (gnuplot == null)
			throw new FileNotFoundException("Can't find GNUPLOT");
	}

	@Override
	public Data process(Data input) {
		try {
			Object urlString = ExpressionResolver.expand(url, context, input);
			if (urlString != null) {
				URL url = new URL(urlString.toString());
				File data = new File(url.toURI());
				createPlot(data);
			} else
				log.error("Url not found!");

		} catch (Exception e) {
			e.printStackTrace();
		}
		return input;
	}

	protected String findGnuplot() {
		for (String path : paths) {
			File file = new File(path + File.separator + "gnuplot");
			if (file.exists() && file.canExecute()) {
				log.debug("Found gnuplot at {}", file.getAbsolutePath());
				return file.getAbsolutePath();
			}
		}

		return null;
	}

	public void createPlot(File file) throws Exception {
		Boolean test = true;
		System.out.println(test);
		String[] names = file.getName().split("\\.");
		String name = file.getName();
		if (names.length != 0)
			name = names[0];
		String cmd = createPlotCommand(name + ".cmd", file);
		Runtime.getRuntime().exec(gnuplot + " " + cmd, new String[0],
				file.getParentFile());
	}

	public String createPlotCommand(String name, File file) throws Exception {
		File plotCmd = new File(file.getParentFile().getAbsolutePath()
				+ File.separator + name);

		PrintStream out = new PrintStream(new FileOutputStream(plotCmd));
		out.println("set terminal png size 1000,400");
		out.println("set grid");

		if (file.getName().endsWith(".dat")) {
			String header = firstLine(file);
			if (header != null) {
				out.println("set output \""
						+ file.getName().replaceAll("dat$", "png") + "\"");
				out.print("plot ");
				String[] cols = header.split(" ");
				for (int i = 1; i < cols.length; i++) {
					out.print("\"" + file.getName() + "\" using 1:" + (i + 1)
							+ " with lines title '" + cols[i] + "'");
					if (i + 1 < cols.length) {
						out.print(", ");
					}
				}
				out.println();
				out.println();
			}
		}

		return plotCmd.getAbsolutePath();
	}

	public String firstLine(File file) throws Exception {
		try {
			BufferedReader r = new BufferedReader(new FileReader(file));
			String line = r.readLine();
			r.close();
			return line;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

}