package stream.plotter.multi;

import javax.swing.JPanel;

import stream.plotter.PlotPanel;

public interface PanelMessageHandler extends MessageHandler {

	public JPanel getPanel();

	public void setPanel(PlotPanel panel);
}
