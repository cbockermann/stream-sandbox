package stream.plotter.multi;

import stream.Data;

public interface MessageHandler {

	public void init();

	public boolean handlesMessage(Data data);

	public void handleMessage(Data data);

	public String[] getKeys();

	public void setKeys(String[] keys);
}
