package stream.plotter.multi;

import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import stream.annotations.Parameter;
import stream.Data;
import stream.plotter.DataVisualizer;

public class MultiMessagePlotter extends DataVisualizer {

	Logger log = LoggerFactory.getLogger(MultiMessagePlotter.class);

	protected boolean keepOpen = false;
	protected JFrame frame;
	protected List<MessageHandler> handlers;
	protected Integer history;

	public MultiMessagePlotter() {
		frame = new JFrame();
		handlers = new ArrayList<MessageHandler>();
	}

	/**
	 * @return the keepOpen
	 */
	public boolean isKeepOpen() {
		return keepOpen;
	}

	/**
	 * @param keepOpen
	 *            the keepOpen to set
	 */
	public void setKeepOpen(boolean keepOpen) {
		this.keepOpen = keepOpen;
	}

	/**
	 * @return the history
	 */
	public Integer getHistory() {
		return history;
	}

	/**
	 * @param history
	 *            the history to set
	 */
	@Parameter(required = false, description = "The number of samples displayed in the plot (i.e. the 'window size' of the plot)")
	public void setHistory(Integer history) {
		this.history = history;
	}

	/**
	 * @see stream.data.ConditionedDataProcessor#processMatchingData(stream.Data)
	 */
	@Override
	public Data processMatchingData(Data data) {
		for (MessageHandler handler : handlers) {
			if (handler.handlesMessage(data)) {
				handler.handleMessage(data);
				break;
			}
		}
		return data;
	}

	/**
	 * @see stream.data.Processor#finish()
	 */
	@Override
	public void finish() throws Exception {
		if (!keepOpen) {
			log.debug("Closing plot frame");
			frame.setVisible(false);
			frame.dispose();
			frame = null;
		} else {
			log.debug("Keeping plot frame visible...");
		}
	}
}
