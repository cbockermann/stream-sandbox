package stream.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import stream.Data;

/**
 * @author Hendrik Blom
 * 
 */
public class SimpleBufferService implements BufferService {

	static Logger log = LoggerFactory.getLogger(SimpleBufferService.class);

	private HashMap<String, List<Data>> buffer;
	private HashMap<String, AtomicInteger> reads;

	public SimpleBufferService() {
		this.buffer = new HashMap<String, List<Data>>();
		this.reads = new HashMap<String, AtomicInteger>();
	}

	@Override
	public List<Data> read(String key) {
		List<Data> result = buffer.get(key);
		AtomicInteger read = reads.get(key);
		if (read == null || read.equals(0)) {
			remove(key);
			log.info("Removed key: {} from buffer", key);
		}
		return result;

	}

	@Override
	public void write(String key, Data data) {
		List<Data> dataList = buffer.get(key);
		if (dataList == null) {
			dataList = new ArrayList<Data>();
			buffer.put(key, dataList);
		}
		dataList.add(data);

	}

	@Override
	public void reset() throws Exception {
		buffer.clear();
	}

	@Override
	public void remove(String key) {
		buffer.remove(key);
	}

	@Override
	public void increaseReads(String key, int number) {
		AtomicInteger read = reads.get(key);
		if (read != null)
			read.addAndGet(number);
	}

}
