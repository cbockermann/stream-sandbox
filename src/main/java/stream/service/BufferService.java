package stream.service;

import java.util.List;

import stream.Data;

/**
 * @author Hendrik Blom
 * 
 */
public interface BufferService extends Service {

	public List<Data> read(String key);

	public void write(String key, Data data);

	public void remove(String key);

	public void increaseReads(String key, int number);

}
