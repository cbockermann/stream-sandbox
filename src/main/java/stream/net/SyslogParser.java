/**
 * 
 */
package stream.net;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import stream.AbstractProcessor;
import stream.Data;
import stream.ProcessContext;
import stream.util.parser.Parser;
import stream.util.parser.ParserGenerator;

/**
 * <p>
 * This parser can read syslog-messages from a byte-array following the syslog
 * format of RFC-5424. By default it expects a byte-array of the raw syslog-data
 * in attribute 'udp:data' (can be changed to another attribute using the 'key'
 * parameter).
 * </p>
 * <p>
 * All values parsed from the input are added to the data item using the
 * 'syslog:' prefix (i.e. syslog:facility, syslog:severity,...). This prefix can
 * be changed using the 'prefix' parameter.
 * </p>
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 * 
 */
public class SyslogParser extends AbstractProcessor {

	static Logger log = LoggerFactory.getLogger(SyslogParser.class);
	String format = "<%(priv)>%(month) %(day) %(time) %(machine) %(process): %(message)";
	String key = "udp:data";
	String prefix = "syslog:";
	String timeFormat = "yyyy MMM dd hh:mm:ss";
	Parser<Map<String, String>> parser;
	SimpleDateFormat timeParser = new SimpleDateFormat(timeFormat);

	/**
	 * @see stream.StatefulProcessor#init(stream.ProcessContext)
	 */
	@Override
	public void init(ProcessContext context) throws Exception {
		parser = (new ParserGenerator(format)).newParser();
		timeParser = new SimpleDateFormat(timeFormat);
	}

	/**
	 * @see stream.Processor#process(stream.data.Data)
	 */
	@Override
	public Data process(Data input) {

		if (input != null) {

			Serializable value = input.get(key);
			if (value == null)
				return input;

			String msg = null;

			try {
				byte[] b = (byte[]) value;
				msg = new String(b);

			} catch (Exception e) {
				e.printStackTrace();
				msg = null;
			}

			if (msg == null) {
				return input;
			}

			try {
				Map<String, String> values = parser.parse(msg);

				String timeString = "2012 " + values.get("month") + " "
						+ values.get("day") + " " + values.get("time");
				log.trace("timeString is: {}", timeString);
				try {
					Long timestamp = timeParser.parse(timeString).getTime();
					input.put(prefix + "timestamp", timestamp);
				} catch (Exception e) {
					log.error("Failed to parse time: {}", e.getMessage());
					if (log.isDebugEnabled())
						e.printStackTrace();
				}

				try {
					if (values.containsKey("priv")) {
						Integer pb = new Integer(values.get("priv").toString());
						int fac = pb / 8;
						int sev = pb - (fac * 8);
						input.put(prefix + "severity", sev);
						input.put(prefix + "facility", fac);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}

				for (String key : values.keySet()) {

					String val = values.get(key);
					if (key.equals("process") && val.indexOf("[") > 0) {
						int idx = val.indexOf("[");
						input.put(prefix + "process", val.substring(0, idx));
						int end = val.indexOf("]", idx);
						input.put(prefix + "pid", val.substring(idx + 1, end));
					} else {
						input.put(prefix + key, values.get(key).trim());
					}
				}

			} catch (Exception e) {
				log.error("Failed to parse syslog-entry: {}", e.getMessage());
				if (log.isDebugEnabled())
					e.printStackTrace();
			}
		}

		return input;
	}

	/**
	 * @return the format
	 */
	public String getFormat() {
		return format;
	}

	/**
	 * @param format
	 *            the format to set
	 */
	public void setFormat(String format) {
		this.format = format;
	}

	/**
	 * @return the key
	 */
	public String getKey() {
		return key;
	}

	/**
	 * @param key
	 *            the key to set
	 */
	public void setKey(String key) {
		this.key = key;
	}

	/**
	 * @return the prefix
	 */
	public String getPrefix() {
		return prefix;
	}

	/**
	 * @param prefix
	 *            the prefix to set
	 */
	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	/**
	 * @return the timeFormat
	 */
	public String getTimeFormat() {
		return timeFormat;
	}

	/**
	 * @param timeFormat
	 *            the timeFormat to set
	 */
	public void setTimeFormat(String timeFormat) {
		this.timeFormat = timeFormat;
	}
}