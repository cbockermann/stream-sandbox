/**
 * 
 */
package stream.image;

import java.net.URL;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import stream.net.UDPStreamTest;

/**
 * @author chris
 * 
 */
public class MJpegImageStreamTest {

	static Logger log = LoggerFactory.getLogger(MJpegImageStreamTest.class);

	@Test
	public void test() {
	}

	public static void main(String[] args) throws Exception {
		URL url = UDPStreamTest.class.getResource("/test-mjpeg-stream.xml");
		log.info("Starting MJpegImageStream-test from {}", url);
		stream.run.main(url);
	}
}
