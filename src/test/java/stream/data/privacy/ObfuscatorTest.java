/**
 * 
 */
package stream.data.privacy;

import junit.framework.Assert;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import stream.ProcessorList;
import stream.Data;
import stream.io.LineStream;

/**
 * @author chris
 * 
 */
public class ObfuscatorTest {

	static Logger log = LoggerFactory.getLogger(ObfuscatorTest.class);

	/**
	 * Test method for
	 * {@link stream.data.privacy.Obfuscator#process(stream.Data)}.
	 */
	@Test
	public void testProcess() throws Exception {

		LineStream stream = new LineStream(
				ObfuscatorTest.class.getResource("/access.log"));

		stream.setFormat("%(REMOTE_ADDR) - - [%(DATE)] \"%(METHOD) %(URI) %(PROTO)\" %(RESPONSE_STATUS) %(RESPONSE_SIZE) \"%(REFERER)\" \"%(USER_AGENT)\"");

		ProcessorList list = new ProcessorList();
		Obfuscator obfuscator = new Obfuscator();
		obfuscator.setKeys(new String[] { "USER_AGENT" });
		list.getProcessors().add(obfuscator);
		obfuscator = new Obfuscator();
		obfuscator.setKeys(new String[] { "REMOTE_ADDR" });
		list.getProcessors().add(obfuscator);

		int i = 0;

		Data item = stream.readNext();
		while (item != null && i++ < 2) {
			log.info("item: {}", item);
			item = list.process(item);
			// log.info("obfuscated: {}", item);
			log.info("Line: {}", item.get("LINE"));

			Assert.assertTrue(item.get("LINE").toString().indexOf("Macintosh") < 0);
			Assert.assertTrue(item.get("LINE").toString()
					.indexOf("129.217.30.156") < 0);
			item = stream.readNext();
		}
	}
}
