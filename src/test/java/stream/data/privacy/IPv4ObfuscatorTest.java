/**
 * 
 */
package stream.data.privacy;

import static org.junit.Assert.fail;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import stream.Data;
import stream.data.DataFactory;

/**
 * @author chris
 * 
 */
public class IPv4ObfuscatorTest {

	static Logger log = LoggerFactory.getLogger(IPv4ObfuscatorTest.class);

	final IPv4Obfuscator obfuscator = new IPv4Obfuscator();

	/**
	 * Test method for
	 * {@link stream.data.privacy.IPv4Obfuscator#obfuscate(stream.Data)}.
	 */
	@Test
	public void testObfuscate() {

		String[] ips = new String[] { "129.217.30.1", "129.217.30.156",
				"129.217.4.42" };

		for (String ip : ips) {
			Data item = DataFactory.create();
			item.put("REMOTE_ADDR", ip);
			log.info("Item: {}", item);
			item = obfuscator.process(item);
			log.info("Obfuscated item: {}", item);
			log.info("--------------------------------------------------------");

			for (String key : item.keySet()) {
				if ((item.get(key) + "").indexOf(ip) >= 0) {
					fail("Found IP in key '" + key + "': " + item.get(key));
				}
			}

		}
	}
}
