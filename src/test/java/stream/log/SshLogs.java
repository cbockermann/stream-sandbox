/**
 * 
 */
package stream.log;

import java.net.URL;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import stream.net.UDPStreamTest;

public class SshLogs {

	static Logger log = LoggerFactory.getLogger(SshLogs.class);

	@Test
	public void test() throws Exception {

		URL url = UDPStreamTest.class.getResource("/test-syslog-stream.xml");
		log.info("Starting syslog-ssh-test from {}", url);
		stream.run.main(url);

	}

	public static void main(String[] args) throws Exception {
		new SshLogs().test();
	}
}
