/**
 * 
 */
package stream.net;

import static org.junit.Assert.fail;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.URL;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import stream.Data;
import stream.data.RemoveKeys;

/**
 * @author chris
 * 
 */
public class UDPStreamTest {

	static Logger log = LoggerFactory.getLogger(UDPStreamTest.class);

	@Test
	public void test() {

		try {
			UDPStream stream = new UDPStream();

			stream.setPort(10514);
			stream.setAddress("192.168.128.4");
			stream.init();

			SyslogParser parser = new SyslogParser();
			parser.init(null);

			RemoveKeys rk = new RemoveKeys();
			rk.setKeys("udp:data".split(","));

			while (true) {
				Data item = stream.readNext();
				item = parser.process(item);
				item = rk.process(item);
				log.info("Data: {}", item);
			}

		} catch (Exception e) {
			fail("Error: " + e.getMessage());
			e.printStackTrace();
		}
	}

	public static void main(String args[]) throws Exception {

		URL url = UDPStreamTest.class.getResource("/test-syslog-udp.xml");
		log.info("Starting syslog-test from {}", url);
		stream.run.main(url);

		System.exit(0);

		if (System.getProperty("run-test") == null) {
			(new UDPStreamTest()).test();
		}

		try {
			int port = 10514;

			// Create a socket to listen on the port.
			DatagramSocket dsocket = new DatagramSocket(port);

			// Create a buffer to read datagrams into. If a
			// packet is larger than this buffer, the
			// excess will simply be discarded!
			byte[] buffer = new byte[2048];

			// Create a packet to receive data into the buffer
			DatagramPacket packet = new DatagramPacket(buffer, buffer.length);

			// Now loop forever, waiting to receive packets and printing them.
			while (true) {
				// Wait to receive a datagram
				dsocket.receive(packet);

				// Convert the contents to a string, and display them
				String msg = new String(buffer, 0, packet.getLength());
				System.out.println(packet.getAddress().getHostName() + ": "
						+ msg);

				// Reset the length of the packet before reusing it.
				packet.setLength(buffer.length);
			}
		} catch (Exception e) {
			System.err.println(e);
		}
	}
}
